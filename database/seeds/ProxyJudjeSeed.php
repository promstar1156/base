<?php

use Illuminate\Database\Seeder;
use App\Models\ProxyJudje;
class ProxyJudjeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pj = new ProxyJudje();
        $pj->name = 'http://56819423.swh.strato-hosting.eu/azenv.php';
        $pj->url  = 'http://56819423.swh.strato-hosting.eu/azenv.php';
        $pj->save();
    }
}
