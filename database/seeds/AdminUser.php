<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
class AdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User;
        $admin->name  = 'Superadmin';
        $admin->email = 'admin@admin.com';
        $admin->password = Hash::make('123456');
        $admin->role = 'superadmin';
        $admin->save();
    }
}
