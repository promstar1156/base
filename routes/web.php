<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
//************************************************************

// Proxy Routes
Route::get('/proxylist', 'ProxyController@getViewProxyList')->name('getProxylist');
Route::post('/proxylist/getall', 'ProxyController@getProxyTable')->name('getProxyTable');
Route::post('/proxylist/refreshProxyInfo', 'ProxyController@refreshProxyInfo')->name('refreshProxyInfo');
// Proxy Judje Routes
Route::get('/proxyjudje', 'ProxyJudjeController@getViewProxyJudjeList')->name('getProxyJudjelist');
Route::post('/proxyjudje/getall', 'ProxyJudjeController@getProxyJudjeTable')->name('getProxyJudjeTable');
Route::post('/proxyjudje/new', 'ProxyJudjeController@createProxyJudge')->name('createProxyJudge');
Route::post('/proxyjudje/delete', 'ProxyJudjeController@deleteProxyJudge')->name('deleteProxyJudge');
// Proxy setup page routes (DEV)
Route::get('/proxysetup', 'ProxyController@getViewProxySetup')->name('getViewProxySetup');
Route::post('/proxysetup/import', 'ProxyController@ProcessProxyImport')->name('ProcessProxyImport');
// Log system path
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logsPage');
// Parser routes
Route::resource('admin/feed', 'Parser\ParserSetupController',  [
    'names' => [
        'index'   => 'feed.get.view',
        'create'  => 'feed.get.create',
        'store'   => 'feed.post.new',
        'show'    => 'feed.get.one',
        'edit'    => 'feed.edit.one',
        'update'  => 'feed.update',
        'destroy' => 'feed.delete'
    ]
]);
Route::post('admin/feed/checksourceajax', 'Parser\ParserSetupController@ajaxCheckSource')->name('feed.ajax.check');
Route::post('admin/feed/testlinksave', 'Parser\ParserSetupController@ajaxSaveTestLink')->name('feed.ajax.testlink');
Route::post('admin/feed/testselector', 'Parser\ParserSetupController@ajaxTestSelector')->name('feed.ajax.testselector');
Route::post('admin/feed/saveselector', 'Parser\ParserSetupController@ajaxSaveSelector')->name('feed.ajax.saveselector');
Route::post('admin/feed/deleteselector', 'Parser\ParserSetupController@ajaxDeleteSelector')->name('feed.ajax.deleteselector');
Route::post('admin/feed/togglestatus', 'Parser\ParserSetupController@ajaxToggleStatus')->name('feed.ajax.togglestatus');
Route::post('admin/feed/savesheetlink', 'Parser\ParserSetupController@ajaxSaveSheetLink')->name('feed.ajax.savesheetlink');
Route::post('admin/feed/addqueue', 'Parser\ParserSetupController@AddToQueue')->name('feed.addtoqueue');