<?php

namespace App\Console\Commands;

use App\Jobs\CheckProxyJob;
use Illuminate\Console\Command;
use App\Http\Classes\Proxy\PJ;
use App\Models\Proxy;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Laravel\Facades\Telegram;

class checkproxy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkproxy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check proxy using proxy judge server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $proxies = Proxy::whereStatus(1)->inRandomOrder()->take(20)->get();
        $bar = $this->output->createProgressBar(20);
        foreach ($proxies as $proxy) {
            CheckProxyJob::dispatch($proxy);
        }
        $bar->finish();
        $this->info("\n");
        $this->info("COMPLETED");
        return true;
    }
}
