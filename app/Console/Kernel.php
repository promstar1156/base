<?php

namespace App\Console;

use App\Http\Classes\Proxy\ProxySourceProxyGather;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;
use App\Http\Classes\Proxy\ProxySource;
use App\Http\Classes\Proxy\ProxySourceProxyListen;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
           // ProxySourceProxyListen::getSource();
          //  ProxySourceProxyGather::getSource();
        })->everyMinute();
       //  $schedule->command('checkproxy')->everyTenMinutes();
       //  $schedule->command('queue:work')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
