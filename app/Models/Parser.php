<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Parser extends Model
{
    public function Selectors()
    {
        return $this->hasMany('App\Models\ParserParameters');
    }
    protected static function boot() {
        parent::boot();
        Parser::deleted(function ($parser) {
            $parser->Selectors()->delete();
        });
    }
}

