<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 12/30/2018
 * Time: 10:27 PM
 */

namespace App\Http\ViewComposers;
use App\Models\Parser;
use Illuminate\Contracts\View\View;
class Sidebar
{
    public function compose(View $view)
    {
        $parsers = Parser::all();
        return $view->with('parsers', $parsers);
    }
} 