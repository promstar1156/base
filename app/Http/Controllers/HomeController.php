<?php

namespace App\Http\Controllers;

use App\Http\Classes\Parser\ParserHelper;
use App\Model\Parser;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as gruzzle;
use App\ProxySource;
use App\Models\Proxy;
use Symfony\Component\DomCrawler\Crawler;
use App\Models\ProxyCountry;
use App\Http\Classes\Proxy\PJ;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Http\Classes\Proxy\ProxySourceProxyGather;
use Symfony\Component\CssSelector\CssSelectorConverter;
use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use App\Jobs\ParseLinkJob;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Log;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.index');
    }

}
