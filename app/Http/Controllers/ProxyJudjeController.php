<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Proxy;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Models\ProxyJudje;

class ProxyJudjeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application product page
     *
     * @return \Illuminate\Http\Response
     */
    public function getViewProxyJudjeList()
    {
        if (Auth::check()) {
            if(Auth::user()->role == 'superadmin') {
                return view('admin.pages.proxyJudje');
            } else {
                return redirect('/');
            }
        }
    }

    public function getProxyJudjeTable(Request $request)
    {
        $total_rows = ProxyJudje::count();
        return Datatables::of(ProxyJudje::query())->addColumn('action', function ($product) {
            return '<button type="button" data-id='.$product->id.' class="btn btn-link btn-xs btn-success btn-just-icon edit">
                      <i class="material-icons">edit</i>
                    </button>
                  <button type="button" data-id='.$product->id.' class="btn btn-link btn-danger btn-just-icon delete">
                     <i class="material-icons">close</i>
                    </button>';
        })->setTotalRecords($total_rows)->setFilteredRecords($total_rows)->make(true);
    }

    public function createProxyJudge(Request $request) {
        $proxy = new ProxyJudje();
        $proxy->name = $request->pj_name;
        $proxy->url  = $request->pj_url;
        $proxy->save();
        return response()->json(['msg' => 'success'], 200);
    }
    
    public function updateProxyJudgeTable(Request $request)
    {
    	$row = ProxyJudje::findOrFail($request->$id);
    	$row->name = $request->name;
    	$row->url  = $request->url;
    	$row->save();
    	return response()->json(['msg' => 'success'], 200);
    }
    
    public function deleteProxyJudge(Request $request){
    	ProxyJudje::destroy($request->id);
    	return response()->json(['msg' => 'success', 200]);
    }

    public function getOneProxyJudge(Request $request){
    	$judge = findOrFail($request->id);
    	return response()->json($judge, 200);
    }
}