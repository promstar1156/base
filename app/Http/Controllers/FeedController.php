<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
use Yajra\DataTables\DataTables;
class FeedController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application product page
     *
     * @return \Illuminate\Http\Response
     */
    public function getFeedView()
    {
        if (Auth::check()) {
            if(Auth::user()->role == 'superadmin') {
                return view('admin.pages.feed');
            } else {
                return redirect('/');
            }
        }
    }


}
