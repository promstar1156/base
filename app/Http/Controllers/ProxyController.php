<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Proxy;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ProxyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application product page
     *
     * @return \Illuminate\Http\Response
     */
    public function getViewProxyList()
    {
        if (Auth::check()) {
            if(Auth::user()->role == 'superadmin') {
                $total = Proxy::count();
                $active = Proxy::where('status', '=', '1')->count();
                $graph_data = DB::table('proxies')
                    ->select('created_at', DB::raw('count(*) as total'))
                    ->where('created_at', '>=', Carbon::now()->subDays(30))
                    ->orderBy('created_at', 'DESC')
                    ->groupBy('created_at')->take(7)
                    ->get()->toArray();

                if (is_array($graph_data)) {
                    foreach ($graph_data as $item) {
                        $graph_labels[] = Carbon::parse($item->created_at)->toTimeString();
                        $graph_values[] = $item->total;
                    }
                }
                $data = [
                    'total_proxy'  => $total,
                    'active_proxy' => $active,
                    'graph_labels' => json_encode($graph_labels),
                    'graph_values' => json_encode($graph_values)
                ];
                return view('admin.pages.proxylist', $data);
            } else {
                return redirect('/');
            }
        }
    }

    public function getProxyTable(Request $request)
    {
        $total_rows = Proxy::count();
        return Datatables::of(Proxy::query())->addColumn('st', function ($proxy) {
            if ($proxy->status == 1) {
                $status = 'done_outline';
                $class  = 'btn-success';
            } else {
                $status = 'error_outline';
                $class  = 'btn-danger';
            }
            return '<button type="button" class="btn btn-link btn-xs '.$class.' btn-just-icon edit">
                      <i class="material-icons">'.$status.'</i>
                    </button>';
        })->rawColumns(['st'])->setTotalRecords($total_rows)->setFilteredRecords($total_rows)->make(true);
    }

    public function getViewProxySetup()
    {
        return view('admin.pages.proxysetup');
    }

    public function refreshProxyInfo()
    {
        $total  = Proxy::count();
        $active = Proxy::where('status', '=', '1')->count();
        $graph_data = DB::table('proxies')
            ->select('created_at', DB::raw('count(*) as total'))
            ->where('created_at', '>=', Carbon::now()->subDays(30))
            ->orderBy('created_at', 'DESC')
            ->groupBy('created_at')->take(7)
            ->get()->toArray();
        foreach ($graph_data as $item) {
            $graph_labels[] = Carbon::parse($item->created_at)->toTimeString();
            $graph_values[] = $item->total;
        }

        return response()->json(['total' => $total,
                                 'active' => $active,
                                 'graph_data' => json_encode($graph_values),
                                 'graph_labels' => json_encode($graph_labels)
                                ], 200);
    }

    public function ProcessProxyImport(Request $request)
    {
        foreach ($request->file() as $file) {
            $file->move(storage_path('app/proxy'), $file->getClientOriginalName());
            $filename = $file->getClientOriginalName();
        }
        $proxy_list = Storage::get('/proxy/'.$filename);
        $lines = explode("\r\n", $proxy_list);
        $total = count($lines);
        $i = 0;
        foreach ($lines as $line) {
           $i++;
           $line = explode(':', $line);
           $check = Proxy::whereIp($line[0])->get()->count();
           if ($check > 0) {
               $proxy           = new Proxy();
               $proxy->ip       = $line[0];
               $proxy->port     = $line[1];
               $proxy->protocol = 'undefined';
               $proxy->status   = '1';
               $proxy->source   = 'list';
               $proxy->save();
           }
        }
        File::put(storage_path('/app/info').'/status.json', json_encode(['total' => $total, 'current' => $i]));
    }
}
