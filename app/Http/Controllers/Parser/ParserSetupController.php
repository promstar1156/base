<?php

namespace App\Http\Controllers\Parser;

use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Parser;
use App\Models\ParserParameters;
use App\Helpers\FlashMessage;
use App\Helpers\GoogleHelper;
use App\Http\Classes\Parser\ParserHelper;
use App\Jobs\ParseLinkJob;

class ParserSetupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.parser.newfeed');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parser = new Parser();
        $parser->name = $request->name;
        $parser->base_url = $request->base_url;
        $parser->source_type = $request->source_type;
        $parser->priority = $request->priority;
        if ($request->useproxy) {
            $parser->useproxy = true;
        } else {
            $parser->useproxy = false;
        }
        $parser->save();
        FlashMessage::flash('Источник успешно добавлен!');
        return redirect()->route('feed.get.one', $parser->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $out   = [];
        $out['sitemap'] = '';
        $parser = Parser::findOrFail($id);

        $out['parser'] = $parser;
        if ($parser->source_type == 'sitemap.xml') {
            $sitemap = ParserHelper::getSitemap($parser->base_url.$parser->source_type);
        } else {
            $sitemap = ParserHelper::getSitemap($parser->source_type);
        }
        if ($sitemap) {
            $links = $sitemap->count();
            $out['sitemap'] = $links;
        }

        $out['robots'] = ParserHelper::getRobots($parser->base_url);
        $out['selectors'] = Parser::find($id)->Selectors;
        return view('admin.pages.parser.editfeed', $out);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $parser = Parser::findOrFail($id);
        $parser->name = $request->name;
        $parser->base_url = $request->base_url;
        $parser->source_type = $request->source_type;
        if ($request->useproxy) {
            $parser->useproxy = true;
        } else {
            $parser->useproxy = false;
        }
        $parser->priority = $request->priority;
        $parser->save();
        FlashMessage::flash('Настройка источника обновлена!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Parser::destroy($id);
        FlashMessage::flash('Источник удален!');
        return redirect()->route('home');
    }

    public function ajaxCheckSource(Request $request)
    {
        $status = ParserHelper::checkConnection($request->base_url);
        if (!$status) {
            $status = 404;
        }
        return response()->json(['code' => $status], 200);
    }

    public function ajaxSaveTestLink(Request $request)
    {
        $parser = Parser::findOrFail($request->id);
        $parser->testlink = $request->link;
        $parser->save();
        return response()->json(['status' => 1], 200);
    }

    public function ajaxTestSelector(Request $request)
    {
        if ($request->retest == 1) {
            if($request->id) {
                $selector = ParserParameters::find($request->id);
                $result   = ParserHelper::getResult($request->link, $selector->selector, $selector->type, $selector->item);

            }
        } else {
            $result = ParserHelper::getResult($request->link, $request->selector,$request->type, $request->item);
        }
        if (!$result) {
            $status = 0;
        } else {
            $status = 1;
        }
        return response()->json(['data' => $result, 'status' => $status], 200);
    }

    public function ajaxSaveSelector(Request $request)
    {
        $selectors = Parser::find($request->id)->Selectors()->where('name', '=', $request->name)->get();
        if ($selectors->count() == 0) {
            // add new one
            $save = new ParserParameters();
            $save->parser_id = $request->id;
            $save->name      = $request->name;
            $save->selector  = $request->selector;
            $save->type      = $request->type;
            $save->item      = $request->item;
            $save->save();
            return response()->json(['dublicate' => '0'],'200');
        } else {
            return response()->json(['dublicate' => '1'],'200');
        }
    }

    public function ajaxDeleteSelector(Request $request)
    {
        ParserParameters::destroy($request->selector_id);
        return response()->json(['msg' => 'success'],'200');
    }

    public function ajaxToggleStatus(Request $request)
    {
        $parser = Parser::findOrFail($request->id);
        $parser->active = $request->status;
        $parser->save();
        return response()->json(['msg' => 'success', 'updated' => $parser->updated_at], 200);
    }

    public function ajaxSaveSheetLink(Request $request)
    {
        $parser = Parser::findOrFail($request->id);
        $code = GoogleHelper::getSheetIdFromLink($request->link);
        if (!$code) {
            return response()->json(['msg' => 'failed', 'reason' => 'invalid link'], 200);
        }
        $check = GoogleHelper::validateSheetAccess($code);
        if (isset($check->error->code)) {
            return response()->json(['msg' => 'failed', 'reason' => $check->error->status], 200);
        }
        $parser->sheetcode = $code;
        $parser->save();
        return response()->json(['msg' => 'success', 'link' => $request->link]);
    }

    public function AddToQueue(Request $request)
    {
        set_time_limit(30000);
        $parser = Parser::findOrFail($request->id);
        if ($parser->source_type == "sitemap.xml") {
            $url = $parser->base_url.$parser->source_type;
        } else {
            $url = $parser->source_type;
        }
        $links = ParserHelper::parseSitemap($url);
        $i = 0;
        if (is_array($links)) {
            foreach ($links as $link) {
                ParseLinkJob::dispatch($parser, $link);
                $i++;
            }
        }
        FlashMessage::flash('Успешно! - добавлено '.$i.' ссылок');
        return redirect()->back();
    }
}
