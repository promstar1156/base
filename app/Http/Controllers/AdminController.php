<?php

namespace App\Http\Controllers;
use PHPHtmlParser\Dom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as gruzzle;
use App\ProxySource;
use App\Models\Proxy;
use Symfony\Component\DomCrawler\Crawler;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function HomeView()
    {
        if (Auth::check()) {
            if(Auth::user()->role == 'superadmin') {
                return view('admin.pages.index');
            } else {
                return redirect('/');
            }
        }
    }

}
