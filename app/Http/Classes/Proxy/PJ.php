<?php

namespace App\Http\Classes\Proxy;
use App\Models\ProxyJudje;
use App\Models\Proxy;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException as GE;
use GuzzleHttp\Exception\BadResponseException as BRE;
use GuzzleHttp\Exception\ConnectException as CE;
use GuzzleHttp\Client as gruzzle;
use Symfony\Component\DomCrawler\Crawler;



class PJ
{
    public static function CheckProxy($ip, $port, $id)
    {
        $Judge = ProxyJudje::inRandomOrder()->first();
        $client = new gruzzle(['verify' => false]);
        try {
            $proxy_adress['proxy'] = $ip.':'.$port;
            $result = $client->request('GET', $Judge->url, $proxy_adress);
            if ($result->getStatusCode() != '200') {
                $upd = Proxy::find($id);
                $upd->status = 0;
                $upd->save();
                return false;
            }
            return true;
        } catch (GE $exception) {
            if ($exception->getCode() == 0) {
                $upd = Proxy::find($id);
                $upd->status = 0;
                $upd->save();
                return false;
            }
        }
        return true;
    }

}