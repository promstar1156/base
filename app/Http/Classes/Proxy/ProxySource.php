<?php


namespace App\Http\Classes\Proxy;


abstract class ProxySource
{
    abstract public static function getSource($name, $useproxy);
}