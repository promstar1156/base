<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 03.02.2019
 * Time: 13:48
 */

namespace App\Http\Classes\Proxy;
use App\Models\Proxy;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException as GE;
use GuzzleHttp\Exception\BadResponseException as BRE;
use GuzzleHttp\Client as gruzzle;
use Symfony\Component\DomCrawler\Crawler;
use App\Models\ProxyCountry;
use Telegram\Bot\Laravel\Facades\Telegram;
class ProxySourceProxyGather extends ProxySource
{
    public static function getSource($name = '', $useproxy = 1) {
        $client = new gruzzle(['verify' => false]);
        $result = $client->request('GET', 'http://www.gatherproxy.com/');
        $body = $result->getBody();
        $content = $body->getContents();
        $crawler = new Crawler($content);
        $crawler = $crawler->filter('script');
        $inner_html = '';
        foreach ($crawler as $item) {
            $inner_html .= $item->ownerDocument->saveXML($item);
        }
        $raw_html_chunks = explode('<script type="text/javascript">' , $inner_html);
        unset($raw_html_chunks[0]);

        foreach ($raw_html_chunks as $one) {
            $ip_start_pos = strpos($one, '"PROXY_IP":"') + 12;
            $ip_end_pos   = strpos($one, '"', $ip_start_pos);
            $ip = substr($one, $ip_start_pos, $ip_end_pos - $ip_start_pos);
            if (strlen($ip) > 17) {
                break;
            }
            $port_start_pos   = strpos($one, '"PROXY_PORT":"') + 14;
            $port_end_pos = strpos($one, '"', $port_start_pos);
            $port = substr($one, $port_start_pos, $port_end_pos - $port_start_pos);
            $port = hexdec($port);

            $country_start_pos   = strpos($one, '"PROXY_COUNTRY":"') + 17;
            $country_end_pos = strpos($one, '"', $country_start_pos);
            $country = substr($one, $country_start_pos, $country_end_pos - $country_start_pos);

            $check = Proxy::whereIp($ip)->get()->count();
            if ($check == 0) {
                Proxy::create([ 'ip' => $ip,
                    'port' => $port,
                    'protocol' => 'https',
                    'country' => $country,
                    'allowshttps' => '',
                    'connect_time' => '',
                    'tfb' => '',
                    'dsp' => '',
                    'status' => 1,
                    'source' => 'www.gatherproxy.com'
                ]);
            }

        }
    }
}