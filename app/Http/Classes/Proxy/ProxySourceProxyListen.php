<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 03.02.2019
 * Time: 13:48
 */

namespace App\Http\Classes\Proxy;
use App\Models\Proxy;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException as GE;
use GuzzleHttp\Exception\BadResponseException as BRE;
use GuzzleHttp\Client as gruzzle;
use Symfony\Component\DomCrawler\Crawler;
use App\Models\ProxyCountry;
use Telegram\Bot\Laravel\Facades\Telegram;
class ProxySourceProxyListen extends ProxySource
{
    public static function getSource($name = '', $useproxy = 1) {
        $client = new gruzzle(['verify' => false]);
        $result = $client->request('GET', 'https://www.proxy-listen.de/Proxy/Proxyliste.html');
        $body = $result->getBody();
        $content = $body->getContents();
        $crawler = new Crawler($content);
        $crawler = $crawler->filter('input[name="fefefsfesf4tzrhtzuh"]');
        $token   = $crawler->attr('value');
        if (!empty($token)) {
            $client = new gruzzle(['verify' => false]);
            $result = $client->request('POST', 'https://www.proxy-listen.de/Proxy/Proxyliste.html', [
                'form_params' => [
                    'filter_port' => '',
                    'filter_http_gateway' => '',
                    'filter_http_anon' => '',
                    'filter_response_time_http' => 1,
                    'fefefsfesf4tzrhtzuh' => $token,
                    'filter_country' => '',
                    'filter_timeouts1' => '',
                    'liststyle' => 'info',
                    'proxies' => '300',
                    'type' => 'https',
                    'submit' => 'Show'
                ]
            ]);
            $body = $result->getBody();
            $content = $body->getContents();
            $crawler = new Crawler($content);
            $crawler = $crawler->filter('tr .proxyListOdd');
            $j = 0;
            $row_arr = [];
            foreach ($crawler as $i => $row) {
                $j++;
                $tr = new Crawler($row);
                $tds = [];
                foreach ($tr->filter('td') as $k => $node) {
                    if ($k == 5) {
                        $img = new Crawler($node);
                        try {
                            $country = $img->filter('img')->attr('title');
                            $check_country = ProxyCountry::whereName_de($country)->get()->first();
                            if ($check_country) {
                                $country = $check_country->code;
                            } else {
                                $country = '';
                            }
                            $tds[$k] = $country;
                        } catch (\InvalidArgumentException $e) {
                            $tds[$k] = "";
                        }
                    } else {
                        if (!empty($node->nodeValue)) {
                            $tds[$k] = $node->nodeValue;
                        } else {
                            $tds[$k] = '';
                        }
                    }
                }
                $row_arr[] = $tds;
            }
            $i=0;
            foreach ($row_arr as $item) {
                $check = Proxy::whereIp($item[0])->get()->count();
                if ($check == 0) {
                    $i++;
                    Proxy::create([ 'ip' => $item[0],
                        'port' => $item[1],
                        'protocol' => 'https',
                        'country' => $item[5],
                        'allowshttps' => '',
                        'connect_time' => '',
                        'tfb' => '',
                        'dsp' => '',
                        'status' => 1
                    ]);
                }
            }
            if ($i > 0) {
                $message = 'Добавлено - '.$i.' новых прокси серверов';
                Telegram::sendMessage([
                    'chat_id' => '110410804',
                    'text' => $message
                ]);
            }

            $client = new gruzzle(['verify' => false]);
            $result = $client->request('POST', 'https://www.proxy-listen.de/Proxy/Proxyliste.html', [
                'form_params' => [
                    'filter_port' => '',
                    'filter_http_gateway' => '',
                    'filter_http_anon' => '',
                    'filter_response_time_http' => 1,
                    'fefefsfesf4tzrhtzuh' => $token,
                    'filter_country' => '',
                    'filter_timeouts1' => '',
                    'liststyle' => 'info',
                    'proxies' => '300',
                    'type' => 'https',
                    'submit' => 'Show'
                ]
            ]);
            $body = $result->getBody();
            $content = $body->getContents();
            $crawler = new Crawler($content);
            $crawler = $crawler->filter('tr .proxyListEven');
            $j = 0;
            $row_arr = [];
            foreach ($crawler as $i => $row) {
                $j++;
                $tr = new Crawler($row);
                $tds = [];
                foreach ($tr->filter('td') as $k => $node) {
                    if ($k == 5) {
                        $img = new Crawler($node);
                        try {
                            $country = $img->filter('img')->attr('title');
                            $check_country = ProxyCountry::whereName_de($country)->get()->first();
                            if ($check_country) {
                                $country = $check_country->code;
                            } else {
                                $country = '';
                            }
                            $tds[$k] = $country;
                        } catch (\InvalidArgumentException $e) {
                            $tds[$k] = "";
                        }
                    } else {
                        if (!empty($node->nodeValue)) {
                            $tds[$k] = $node->nodeValue;
                        } else {
                            $tds[$k] = '';
                        }
                    }
                }
                $row_arr[] = $tds;
            }
            $i=0;
            foreach ($row_arr as $item) {
                $check = Proxy::whereIp($item[0])->get()->count();
                if ($check == 0) {
                    $i++;
                    Proxy::create([ 'ip' => $item[0],
                        'port' => $item[1],
                        'protocol' => 'https',
                        'country' => $item[5],
                        'allowshttps' => '',
                        'connect_time' => '',
                        'tfb' => '',
                        'dsp' => '',
                        'status' => 1,
                        'source' => 'ProxyListen.de'
                    ]);
                }
            }
            log::info('NEW PROXY'.':'.$i);
            if ($i > 0) {
                $message = 'Добавлено - '.$i.' новых прокси серверов';
                Telegram::sendMessage([
                    'chat_id' => '110410804',
                    'text' => $message
                ]);
            }
        }
    }
}