<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 03.02.2019
 * Time: 10:01
 */

namespace App\Http\Classes\Proxy;
use App\Proxy;
use App\ProxySource;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException as GE;
use GuzzleHttp\Exception\BadResponseException as BRE;
use GuzzleHttp\Client as gruzzle;

class ProxySourceAPI extends ProxySource
{

    public static function getSource($name, $useproxy = 1)
    {
        $proxy = ProxySource::whereName($name)->get()->first();

        if ($proxy->blocked == 0) {
            if ($proxy->waitfor == 0) {
                // no need to wait
                // iterate requests counter
                $proxy_adress = [];
                if ($useproxy == 1) {
                    $get_proxy = Proxy::orderByRaw('RAND()')->take(1)->get()->first();
                    $proxy_adress['proxy'] = $get_proxy->ip.':'.$get_proxy->port;
                }

                $proxy->all_requests = $proxy->all_requests++;
                $client = new gruzzle();
                try {
                        $result = $client->request('GET', $proxy->api_link, $proxy_adress);
                        dd($result);
                        if ($result->getStatusCode() == "200") {
                            $body = $result->getBody();
                            log::info('WORKED 200');
                            $proxy->all_requests = $proxy->all_requests++;
                            $body = json_decode($body->getContents());
                            dd($body);
                        } else {
                            log::info(date('y-m-d h:m:i').'RES:'.$body);
                        }
                } catch (BRE $e) {
                    $response = $e->getResponse();
                    switch ($response->getStatusCode()) {
                        case 429:
                            self::getSource($name, 1);
                            break;
                        case 403:
                            break;
                        case 404:
                            break;
                        case 500:
                            break;
                    }
                }
            } else {
                // wait for it
                log::info('WORKED BLOCKED');
                $proxy->all_requests = $proxy->all_requests++;
                $proxy->waitfor = (int)$proxy->waitfor - 1 ;
                $proxy->save();
            }
        }
    }
}