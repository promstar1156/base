<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 02.06.2019
 * Time: 21:02
 */

namespace App\Helpers;
use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use Google_Service_Exception;
use Illuminate\Support\Facades\Log;

class GoogleHelper
{
    public static function getSheetIdFromLink(String $link)
    {
       $link_parts =  explode('/', $link);
       if ($link_parts[4] == 'd') {
           return $link_parts[5];
       }
       return false;
    }

    public static function validateSheetAccess(String $sheet_id)
    {
        putenv('GOOGLE_APPLICATION_CREDENTIALS=../storage/ringed-glass-229810-38a8970f08d5.json');
        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->addScope(Google_Service_Sheets::SPREADSHEETS);
        $service = new Google_Service_Sheets( $client );
        try  {
            $response = $service->spreadsheets->get($sheet_id);
        } catch (Google_Service_Exception $exception) {
            return \GuzzleHttp\json_decode($exception->getMessage());
            Log::info($exception->getMessage());
        }
    }

    public static function appendRows(array $values, string $spreadsheetId)
    {
        //
        //$service = self::getClient();
        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->addScope(Google_Service_Sheets::SPREADSHEETS);
        $service = new Google_Service_Sheets( $client );
        $range = 'A1';
        $body    = new Google_Service_Sheets_ValueRange( [ 'values' => $values ] );
        $options = array( 'valueInputOption' => 'RAW' );
        $service->spreadsheets_values->append( $spreadsheetId, $range, $body, $options );
    }

    private static function getClient()
    {
        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->addScope(Google_Service_Sheets::SPREADSHEETS);
        $service = new Google_Service_Sheets( $client );
        return $service;
    }
}