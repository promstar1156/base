<?php

namespace App\Http\Classes\Parser;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException as GE;
use GuzzleHttp\Exception\BadResponseException as BRE;
use GuzzleHttp\Exception\ConnectException as CE;
use GuzzleHttp\Client as gruzzle;
use stringEncode\Exception;
use Illuminate\Support\Str;
use SimpleXMLElement;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\CssSelector\CssSelectorConverter;
/**
 * Class Parser
 * @package App\Http\Classes\Parser
 */
class ParserHelper
{

    /**
     * @param string $url
     * @return int
     * @throws GE
     */
    public static function checkConnection(string $url)
    {
        $client = new gruzzle();
        try {
            $result = $client->request('GET', $url);
            $code = $result->getStatusCode();
            return $code;
        } catch(\Exception $e) {
            return false;
        }
    }

    /**
     * @param string $url
     * @return bool|SimpleXMLElement
     */
    public static function getSitemap(string $url)
    {
        try {
            $client = new gruzzle();
            $response = $client->get($url);
            $sitemap = new SimpleXMLElement($response->getBody()->getContents());
            return $sitemap;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $url
     * @return array
     */
    public static function parseSitemap($url)
    {
        $sitemap = self::getSitemap($url);
        $links = self::getSitemapLinks($sitemap);
        return $links;
    }

    /**
     * @param $sitemap
     * @return array
     */
    public static function getSitemapLinks($sitemap)
    {
        foreach ($sitemap as $one) {
            $links[] = $one->loc->__toString();
        }
        return $links;
    }

    /**
     * @param SimpleXMLElement $xml
     * @return array
     */
    public static function xmlToArray(SimpleXMLElement $xml): array
    {
        $parser = function (SimpleXMLElement $xml, array $collection = []) use (&$parser) {
            $nodes = $xml->children();
            $attributes = $xml->attributes();

            if (0 !== count($attributes)) {
                foreach ($attributes as $attrName => $attrValue) {
                    $collection['attributes'][$attrName] = strval($attrValue);
                }
            }

            if (0 === $nodes->count()) {
                $collection['value'] = strval($xml);
                return $collection;
            }

            foreach ($nodes as $nodeName => $nodeValue) {
                if (count($nodeValue->xpath('../' . $nodeName)) < 2) {
                    $collection[$nodeName] = $parser($nodeValue);
                    continue;
                }

                $collection[$nodeName][] = $parser($nodeValue);
            }

            return $collection;
        };

        return [
            $xml->getName() => $parser($xml)
        ];
    }

    /**
     * @param string $url
     * @param string $selector
     * @param string $type
     * @param string $item
     * @return bool|string|null
     * @throws GE
     */
    public static function getResult(string $url, string $selector, string $type = 'xpath', string $item = 'text')
    {
        $client  = new gruzzle(['verify' => false]);
        $result  = $client->request('GET', $url);
        $body    = $result->getBody();
        $content = $body->getContents();
        $crawler = new Crawler($content);
        if ($type == 'xpath') {
            // use xpath
            $xpath = $selector;
        } else {
            // convert css to xpath
            $toXpath = new CssSelectorConverter();
            $xpath   = $toXpath->toXPath($selector);
        }

        $crawler = $crawler->filterXPath($xpath);
        if ($crawler->count() > 0) {
            // if not empty
            switch ($item) {
                case "text":
                    return  $crawler->text();
                    break;
                case "html":
                    // dev
                    $html = $crawler->html();
                    return $html;
                    break;
                case "image":
                    // dev
                    return false;
                    break;
                case "attribute":
                    // dev
                    return $crawler->attr('src');
                    break;
                default:
                    return false;
                    break;
            }
        } else {
            return false;
        }
    }

    /**
     * @param string $url
     * @return string
     * @throws GE
     */
    public static function getPage(string $url)
    {
        $client  = new gruzzle(['verify' => false]);
        $result  = $client->request('GET', $url);
        $body    = $result->getBody();
        $content = $body->getContents();
        return $content;
    }

    /**
     * @param $page
     * @param $selector
     * @param string $type
     * @param string $item
     * @return bool|string|null
     */
    public static function ParsePage($page, $selector, $type = 'xpath', string $item = 'text')
    {
        $crawler = new Crawler($page);
        if ($type == 'xpath') {
            // use xpath
            $xpath = $selector;
        } else {
            // convert css to xpath
            $toXpath = new CssSelectorConverter();
            $xpath   = $toXpath->toXPath($selector);
        }
        $crawler = $crawler->filterXPath($xpath);
        if ($crawler->count() > 0) {
            // if not empty
            switch ($item) {
                case "text":
                    return  $crawler->text();
                    break;
                case "html":
                    // dev
                    return $crawler->text();
                    break;
                case "image":
                    return false;
                    break;
                case "attribute":
                    return $crawler->attr('src');
                    break;
                default:
                    return false;
                    break;
            }
        } else {
            return false;
        }
    }

    /**
     * @param string $url
     * @return bool|string
     */
    public static function getRobots(string $url)
    {
        $url = self::fixUrl($url);
        try {
            $client = new gruzzle();
            $response = $client->get($url.'robots.txt');
            $robots =$response->getBody()->getContents();
            return $robots;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param string $url
     * @return string
     */
    public static function fixUrl(string $url)
    {
       return Str::finish($url, '/');
    }
}