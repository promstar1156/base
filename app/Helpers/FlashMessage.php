<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 17.03.2019
 * Time: 11:03
 */

namespace App\Helpers;
use Illuminate\Support\Facades\Session;

class FlashMessage
{
    public static function flash(string $message = 'Операция успешна!', string $type = 'success', string $title = 'Успешно!') {
        Session::flash('status', $type);
        Session::flash('title', $title);
        Session::flash('message', $message);
        return true;
    }
}