<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Parser;
use App\Models\ParserParameters;
use Illuminate\Support\Facades\Log;
use App\Http\Classes\Parser\ParserHelper;
use App\Helpers\GoogleHelper;
class ParseLinkJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $parser;
    protected $link;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Parser $parser, string $link)
    {
       $this->parser = $parser;
       $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       $page = ParserHelper::getPage($this->link);
       $selectors = $this->parser->Selectors()->get();
       $result[] = $this->link;
       foreach ($selectors as $selector) {
          $result[] = ParserHelper::ParsePage($page, $selector->selector, $selector->type, $selector->item);
       }
       $values[] = $result;
       Log::info($result);
       GoogleHelper::appendRows($values, $this->parser->sheetcode);
    }
}
