<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Classes\Proxy\PJ;
use App\Models\Proxy;

class CheckProxyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $proxy;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Proxy $proxy)
    {
        $this->proxy = $proxy;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        PJ::CheckProxy($this->proxy->ip, $this->proxy->port, $this->proxy->id);
    }
}
