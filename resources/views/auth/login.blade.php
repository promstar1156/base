@extends('admin.layouts.login-layout')
@section('content')
<div class="col-lg-4 col-md-6 col-sm-6 ml-auto mr-auto">
    <form class="form" method="POST" action="{{ route('login') }}">
        @csrf
        <div class="card card-login ">
            <div class="card-header card-header-danger text-center">
                <h4 class="card-title">Вход</h4>
                <div class="social-line"></div>
            </div>
            <div class="card-body ">
                <span class="bmd-form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                            <i class="material-icons">email</i>
                            </span>
                        </div>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                    </div>
                </span>
                <span class="bmd-form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">lock_outline</i>
                            </span>
                        </div>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </span>
            </div>
            <div class="card-footer justify-content-center">
                <button type="submit" class="btn btn-rose btn-link btn-lg">
                    Авторизация
                </button>
            </div>
        </div>
    </form>
</div>
@endsection