@component('admin.chunks.head')
@endcomponent
<body class="">
<div class="wrapper">
    <div class="sidebar"
        data-background-color=""
         data-image="img/sidebar-1.jpg">
        @include('admin.chunks.sidebar')
    </div>

    <div class="main-panel">
        <!-- Navbar -->
        @component('admin.chunks.navbar')
        @endcomponent

        <!-- End Navbar -->
        <div class="content">
            <div class="content">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </div>
        </div>
        @component('admin.chunks.footer')
        @endcomponent
    </div>
</div>
</body>
@component('admin.chunks.js')
@endcomponent
@stack('scripts');
</html>
