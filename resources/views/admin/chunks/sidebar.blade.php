<div class="sidebar"
     data-color="danger"
     data-background-color="white"
     data-image="{{ asset('admin/img/sidebar-1.jpg') }}">

    <div class="logo">
        <a href="/" target="_blank" class="simple-text logo-mini">
            UP
        </a>

        <a href="/" target="_blank" class="simple-text logo-normal">
            Dev 0.0.2
        </a>
    </div>

    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{ asset('admin/img/faces/avatar.jpg') }}" alt="avatar"/>
            </div>
            <div class="user-info">
                <a data-toggle="collapse" href="#collapseExample" class="username">
                    <span>
                         @php
                             echo App\User::find(Auth::guard('web')->id())->name;
                         @endphp
                        <b class="caret"></b>
                     </span>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                <span class="sidebar-mini"> В </span>
                                <span class="sidebar-normal">Выход  </span>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
            @if(App\User::find(Auth::guard('web')->id())->role == 'superadmin')
                <li class="nav-item active ">
                    <a class="nav-link" href="{{ route('home') }}">
                        <i class="material-icons">folder</i>
                        <p> Главная </p>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link active" data-toggle="collapse" href="#categories">
                        <i class="material-icons">category</i>
                        <p> Feed
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="categories">
                        <ul class="nav">
                            <li class="nav-item ">
                                <a class="nav-link" href="{{ route('feed.get.view') }}">
                                    <span class="sidebar-mini"><i class="material-icons">add</i></span>
                                    <span class="sidebar-normal"> Добавить </span>
                                </a>
                            </li>
                            @foreach($parsers as $parser)
                            <li class="nav-item ">
                                <a class="nav-link" href="{{ route('feed.get.one', $parser->id) }}">
                                    <span class="sidebar-normal"> {{ $parser->name }} </span>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
                <li class="nav-item active">
                    <a class="nav-link active" data-toggle="collapse" href="#proxy">
                        <i class="material-icons">http</i>
                        <p> Proxy
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="proxy">
                        <ul class="nav">
                            <li class="nav-item ">
                                <a class="nav-link" href="{{ route('getProxylist') }}">
                                    <span class="sidebar-mini"><i class="material-icons">list_alt</i></span>
                                    <span class="sidebar-normal">Список proxy</span>
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="{{ route('getViewProxySetup') }}">
                                    <span class="sidebar-mini"><i class="material-icons">list_alt</i></span>
                                    <span class="sidebar-normal">Источники Proxy</span>
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="{{ route('getProxyJudjelist') }}">
                                    <span class="sidebar-mini"><i class="material-icons">list_alt</i></span>
                                    <span class="sidebar-normal">Proxy Judje</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item active">
                    <a class="nav-link active" data-toggle="collapse" href="#settings">
                        <i class="material-icons">settings</i>
                        <p> Настройки
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="settings">
                        <ul class="nav">
                            <li class="nav-item ">
                                <a class="nav-link" href="{{ route('logsPage') }}">
                                    <span class="sidebar-mini"><i class="material-icons">warning</i></span>
                                    <span class="sidebar-normal">Просмотр событий</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endif
        </ul>
    </div>
</div>