<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <title>@yield('pagetitle')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="{{ asset('admin/fa/css/font-awesome.min.css') }}">
    <!-- CSS Files -->
    <link href="{{ asset('admin/css/material-dashboard.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/css/material-dashboard.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/css/toastr.min.css') }}" rel="stylesheet" />
</head>
