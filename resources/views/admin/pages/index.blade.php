@extends('admin.layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">import_export</i>
                    </div>
                    <p class="card-category">Очереди</p>
                    <h3 class="card-title">9999999</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        Последнее обновление: 18.01.2019 21:00:21
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">equalizer</i>
                    </div>
                    <p class="card-category">Обработано страниц</p>
                    <h3 class="card-title">9999999</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                         Последнее обновление: 18.01.2019 21:00:21
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">http</i>
                    </div>
                    <p class="card-category">Проверенных proxy</p>
                    <h3 class="card-title">+ 999999</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">date_range</i> Последние 24 часа
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                    <div class="card-icon">
                        <i class="fa fa-twitter"></i>
                    </div>
                    <p class="card-category">Активных источников</p>
                    <h3 class="card-title">999999</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">update</i> Только что
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
