@extends('admin.layouts.master')
@section('pagetitle', 'Список прокси')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header card-header-danger card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons"></i>
                    </div>
                    <h4 class="card-title">Информация</h4>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="index_main.blade.php">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td>Всего прокси</td>
                                        <td class="text-right" data-value="{{ $total_proxy }}" id="total_proxy">
                                            {{ $total_proxy }}
                                        </td>
                                        <td class="text-right text-danger">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Активных: </td>
                                        <td class="text-right" data-value="{{ $active_proxy }}" id="proxy_active">
                                            {{ $active_proxy }}
                                        </td>
                                        <td class="text-right text-danger">

                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-8 ml-auto mr-auto">
                            <div class="card">
                                <div class="card-header card-header-danger card-header-icon">
                                        <div class="card-icon">
                                            <i class="material-icons">show_chart</i>
                                        </div>
                                    <h4 class="card-title">Добавленные прокси</h4>
                                </div>
                                <div class="card-body">
                                    <div class="toolbar">
                                        <!--        Here you can write extra buttons/actions for the toolbar              -->
                                    </div>
                                    <div class="ct-chart ct-perfect-fourth" id="pulse_chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-danger card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">format_list_numbered</i>
                    </div>
                    <h4 class="card-title">Список прокси</h4>
                </div>
                <div class="card-body">
                    <div class="toolbar">
                        <!--        Here you can write extra buttons/actions for the toolbar              -->
                    </div>
                    <div class="material-datatables">
                        <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%" id="proxy-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>IP</th>
                                <th>Порт</th>
                                <th>Протокол</th>
                                <th>Страна</th>
                                <th>Статус</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('#proxy-table').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                pageLength: 25,
                order: [[ 0, "desc" ]],
                columnDefs: [

                ],
                language : {
                    url: "//cdn.datatables.net/plug-ins/1.10.13/i18n/Russian.json"
                },
                ajax: {
                    type: 'POST',
                    url: '{{ route('getProxyTable') }}',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'ip', name: 'ip' },
                    { data: 'port', name: 'port' },
                    { data: 'protocol', name: 'protocol'},
                    { data: 'country', name: 'country'},
                    { data: 'st', name: 'st'}
                ]
            });

            var chart =  new Chartist.Line('#pulse_chart', {
                labels: {!! $graph_labels !!},
                series: [
                    {!! $graph_values !!},
                ]
            }, {
                fullWidth: true,
                chartPadding: {
                    right: 60
                },
                low: 0,
                high: Math.max.apply(null, {!! $graph_values !!})+1,
                height: 300,
                axisY: {
                    onlyInteger: true,
                    offset: 10
                }

            });

            setInterval(function() {
                var total = parseInt($('#total_proxy').html());
                var unactive = parseInt($('#proxy_active').html());
                $.ajax({
                    type: "POST",
                    url: '{{ route('refreshProxyInfo') }}',
                    cache: false,
                    dataType: "json",
                    data: {
                        _token:$('meta[name="csrf-token"]').attr('content')
                    }
                }).done(function(r){
                    $('#total_proxy').html(r.total);
                    $('#proxy_active').html(r.active);
                    var diff = parseInt(r.total) - total;
                    if (diff > 0) {
                        toastr.info('Добавлено '+diff+' новых proxy');
                    }
                    var diff_unactive = unactive - parseInt(r.active);
                    if (diff_unactive > 0) {
                        toastr.warning('Отключено '+diff_unactive+' proxy');
                    }
                });
            }, 10000); //
        });
    </script>
@endpush
