@extends('admin.layouts.master')
@section('pagetitle', 'Получение прокси')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="progress progress-line-primary">
            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 5%;">
                <span class="sr-only">60% Complete</span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div>
            <form action="{{ route('ProcessProxyImport') }}" method="POST" enctype="multipart/form-data">
                @csrf
                 <span class="btn btn-danger btn-file">
                <span class="fileinput-new">Выберите файл</span>
                <input type="hidden"><input type="file" name="source">
                <div class="ripple-container"></div></span>
                <button type="submit" class="btn btn-danger">Импорт</button>
            </form>
        </div>
    </div>
</div>
@endsection
