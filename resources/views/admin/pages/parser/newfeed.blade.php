@extends('admin.layouts.master')
@section('pagetitle', 'Настройка источника')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                    <i class="material-icons"></i>
                </div>
                <h4 class="card-title">Добавить новый источник</h4>
            </div>
            <div class="card-body ">
                <div class="row">
                    <form action="{{ route('feed.post.new') }}" method="POST">
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Название источника</label>
                                <input type="text" value="" required name="name" class="form-control" >
                            </div>
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating" id="base_url_label">Базовый URL </label>
                                <input type="text" value="" required name="base_url" id="base_url" class="form-control" >
                            </div>
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Метод поиска ссылок</label>
                                <input type="text" value="sitemap.xml" required name="source_type" class="form-control">
                            </div>
                            <div class="form-group bmd-form-group">
                                <select class="selectpicker"  name="priority" data-style="select-with-transition" title="Приоритет обработки" tabindex="-98">
                                    @for ($i = 0; $i <= 10; $i++)
                                        <option value="{{$i}}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group bmd-form-group">
                                <div class="togglebutton">
                                    <label>
                                        <input type="checkbox" name="useproxy" >
                                        <span class="toggle"></span>
                                    </label>
                                    <span>Использовать Proxy</span>
                                </div>
                            </div>
                            <div class="form-group bmd-form-group">
                                <button class="btn btn-danger" type="submit">Cохранить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
    $(document).on('focusout', '#base_url', function () {
        let base_url = $('#base_url').val();
        $.ajax({
            type: "POST",
            url: '{{ route('feed.ajax.check') }}',
            cache: false,
            dataType: "json",
            data: {
               base_url : base_url,
                _token:$('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (res) {
            if (res.code === 200) {
                $('#base_url_label').html('Базовый URL <span class="material-icons small text-success"></span>' + ' ' + '<span class="text-success">'+res.code+'</span>');
            } else {
                $('#base_url_label').html('Базовый URL <span class="material-icons small text-danger">warning</span>' + ' ' + '<span class="text-danger">'+res.code+'</span>');
            }
        });
    });
</script>
@endpush
