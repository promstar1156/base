@extends('admin.layouts.master')
@section('pagetitle', 'Настройка источника')
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card ">
            <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                    <i class="material-icons"></i>
                </div>
                <h4 class="card-title">Редактировать источник</h4>
            </div>
            <div class="card-body ">
                <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('feed.update', $parser->id) }}" method="POST" >
                                @method('PUT')
                                @csrf
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Название источника</label>
                                <input type="text" value="{{ $parser->name }}" required name="name" class="form-control" >
                            </div>
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating" id="base_url_label">Базовый URL</label>
                                <input type="text" value="{{ $parser->base_url }}" required name="base_url" id="base_url" class="form-control" >
                            </div>
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Метод поиска ссылок</label>
                                <input type="text" value=" {{ $parser->source_type }}" required name="source_type" class="form-control">
                            </div>
                            <div class="form-group bmd-form-group">
                                <select class="selectpicker"  name="priority" data-active="{{ $parser->priority }}" data-style="select-with-transition" title="Приоритет обработки" tabindex="-98">
                                    @for ($i = 0; $i <= 10; $i++)
                                        <option {{ $i == $parser->priority ? 'selected' : '' }} value="{{$i}}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group bmd-form-group">
                                <div class="togglebutton">
                                    <label>
                                        <input type="checkbox" value="1" {{ $parser->useproxy === 1 ? 'checked' : ''  }} name="useproxy" >
                                        <span class="toggle"></span>
                                    </label>
                                    <span>Использовать Proxy</span>
                                </div>
                            </div>
                            <div class="form-group bmd-form-group">
                                <button class="btn btn-danger" type="submit">Cохранить</button>
                                </form>
                                <form action="{{ route('feed.delete', $parser->id) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger" type="submit">Удалить</button>
                                </form>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card ">
            <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                    <i class="material-icons"></i>
                </div>
                <h4 class="card-title">Информация</h4>
            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card ">
                            <div class="card-header card-header-danger card-header-icon">
                                <h3 class="card-title">Статус </h3>
                                <div class="togglebutton toggle_fix">
                                    <label>
                                        <input id="togglestatus" type="checkbox" value="1" {{ $parser->active === 1 ? 'checked' : ''  }}  >
                                        <span class="toggle"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <i class="material-icons text-success">update</i> <span id="status_updated">{{ $parser->updated_at }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card ">
                            <div class="card-header card-header-danger card-header-icon">
                                <h3 class="card-title">Ссылки (SM)</h3>
                                <h4 class="card-title"><span id="sitemap_links">{{ $sitemap }}</span>
                                    @if($sitemap > 0)
                                        <button class="btn btn-danger btn-just-icon btn-xs show_modal_add_queue">+</button>
                                    @else
                                        <span>Карта сайта не найдена</span>
                                    @endif
                                </h4>

                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <i class="material-icons text-success">update</i> {{ $parser->updated_at }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card ">
                            <div class="card-header card-header-danger card-header-icon">
                                <h3 class="card-title">В очереди (DEV) </h3>
                                <h4 class="card-title" id="queue">999999{{ $parser->total_queue }}</h4>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <i class="material-icons text-success">update</i> 18.01.2019 21:00:21
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card ">
                            <div class="card-header card-header-danger card-header-icon">
                                <h3 class="card-title">Таблица</h3>
                                <button class="btn btn-danger btn-just-icon btn-xs show_modal">+</button>
                                <a id="sheetlink" target="_blank" href="https://docs.google.com/spreadsheets/d/{{ $parser->sheetcode }}/">Ссылка</a>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-lg-4 col-md-6 col-sm-6">--}}
                        {{--<div class="card ">--}}
                            {{--<div class="card-header card-header-danger card-header-icon">--}}
                                {{--<h3 class="card-title">Empty (DEV)</h3>--}}
                                {{--<h4 class="card-title">999999</h4>--}}
                            {{--</div>--}}
                            {{--<div class="card-footer">--}}
                                {{--<div class="stats">--}}
                                    {{--<i class="material-icons text-success">update</i> 18.01.2019 21:00:21--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card ">
                            <div class="card-header card-header-danger card-header-icon">
                                <h3 class="card-title">Готово (DEV)</h3>
                                <h4 class="card-title">999999</h4>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <i class="material-icons text-success">update</i> 18.01.2019 21:00:21
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-danger">
                <h4 class="card-title ">Настройка источника
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating text-white">Тестовая ссылка</label>
                        <input type="text" class="form-control" id="testlink" value="{{ $parser->testlink }}">
                    </div>
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary text-center">
                            <tr>
                                <th width="150px">Название</th>
                                <th width="200px">Селектор</th>
                                <th width="50px" style="max-width: 50px; !important;">Тип селектора</th>
                                <th width="50px">Тип содержимого</th>
                                <th>Результат теста</th>
                                <th width="20px"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <input type="text" id="name" class="form-control">
                            </td>
                            <td>
                                <input type="text" id="selector" class="form-control">
                            </td>
                            <td >
                                <select id="type" class="selectpicker" data-width="90px"  data-active="" data-style="select-with-transition" title="" tabindex="-98">
                                    <option value="xpath">XPath</option>
                                    <option value="css">CSS</option>
                                </select>
                            </td>
                            <td>
                                <div class="form-group bmd-form-group">
                                    <select id="item" class="selectpicker" data-width="90px" name="priority" data-active="" data-style="select-with-transition" title="" tabindex="-98">
                                        <option value="text">Текст</option>
                                        <option value="html">HTML</option>
                                        <option value="image">Картинка</option>
                                        <option value="attr">Атрибут</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <span id="result"></span>
                            </td>
                            <td>
                                <button type="button" data-id='' class="btn btn-link btn-xs btn-success btn-just-icon save">
                                    <i class="material-icons">save</i>
                                </button>
                            </td>
                        </tr>
                        @foreach($selectors as $selector)
                            <tr id="row_{{$selector->id}}">
                                <td>
                                    {{ $selector->name }}
                                </td>
                                <td colspan="2s">
                                    <span id="selector_{{$selector->id}}">{!! $selector->selector !!}</span>
                                </td>
                                {{--<td >--}}
                                    {{--<select disabled class="selectpicker" data-width="90px" name="priority" data-active="{{$selector->type}}" data-style="select-with-transition" title="" tabindex="-98">--}}
                                        {{--<option value="xpath">XPath</option>--}}
                                        {{--<option value="css">CSS</option>--}}
                                    {{--</select>--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--<div class="form-group bmd-form-group">--}}
                                        {{--<select disabled class="selectpicker" data-width="90px" name="priority" data-active="{{$selector->item}}" data-style="select-with-transition" title="" tabindex="-98">--}}
                                            {{--<option value="text">Текст</option>--}}
                                            {{--<option value="html">HTML</option>--}}
                                            {{--<option value="image">Картинка</option>--}}
                                            {{--<option value="attribute" >Атрибут</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</td>--}}
                                <td colspan="2">
                                    <span id="result_{{$selector->id}}" ></span>
                                </td>
                                <td>
                                    <button type="button" data-id="{{ $selector->id }}" class="btn btn-link btn-xs btn-success  btn-just-icon test">
                                        <i class="material-icons">refresh</i>
                                    </button>
                                    <button type="button" data-id="{{ $selector->id }}" class="btn btn-link btn-xs btn-danger  btn-just-icon delete">
                                        <i class="material-icons">close</i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-mini modal-primary" id="modal_sheet" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-small">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
            </div>
            <div class="modal-body">
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating">Код таблицы Google</label>
                    <input id="sheet_link" type="text" value="https://docs.google.com/spreadsheets/d/{{ $parser->sheetcode }}/" required  class="form-control" >
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-link" data-dismiss="modal">Закрыть<div class="ripple-container"><div class="ripple-decorator ripple-on ripple-out" style="left: 57.7625px; top: 19px; background-color: rgb(153, 153, 153); transform: scale(12.7954);"></div></div></button>
                <button type="button" class="btn btn-success btn-link save_sheet">Обновить
                    <div class="ripple-container"></div>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal  modal-primary" id="modal_add_queue234" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-small">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
            </div>
            <div class="modal-body">
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating">Добавить - {{$sitemap}} ссылок в очередь обработчика?</label>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-link" data-dismiss="modal">Закрыть<div class="ripple-container"><div class="ripple-decorator ripple-on ripple-out" style="left: 57.7625px; top: 19px; background-color: rgb(153, 153, 153); transform: scale(12.7954);"></div></div></button>
                <button type="button" class="btn btn-success btn-link save_sheet">Добавить
                    <div class="ripple-container"></div>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_add_queue" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Добавить ссылки в очередь обработчика</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">close</i>
                </button>
            </div>
            <form action="{{ route('feed.addtoqueue') }}" method="POST">
            <input type="hidden" name="id" value="{{$parser->id}}">
            @csrf
            <div class="modal-body">
                <div class="instruction">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="description"> Добавить - {{ $sitemap  }} Ссылок в очередь обработчика?</p>
                            <p class="description"> Или выберите файл с ссылками для добавления в очередь (DEV)</p>
                            <input type="file" class="form-control input-group-btn">
                        </div>
                    </div>
                </div>
                <div class="instruction">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="description" id="response"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-success pull-left" data-dismiss="modal">Закрыть</button>
                <button type="submit"  class="btn btn-danger pull-right" id="add_queue">Добавить</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal modal-mini modal-primary" id="modal_messages" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-small">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
            </div>
            <div class="modal-body">
                <div class="form-group bmd-form-group">
                    <p class="text-center" id="msg"></p>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-link" data-dismiss="modal">Закрыть<div class="ripple-container"><div class="ripple-decorator ripple-on ripple-out" style="left: 57.7625px; top: 19px; background-color: rgb(153, 153, 153); transform: scale(12.7954);"></div></div></button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).on('click', '.show_modal_add_queue', function(){
            $('#modal_add_queue').modal('show');
        });

        $(document).on('click', '.show_modal', function(){
            $('#modal_sheet').modal('show');
        });

        $(document).on('click', '.save_sheet', function(){
            let link = $('#sheet_link').val();
            let id = '{{$parser->id}}';
            $.ajax({
                type: "POST",
                url: '{{ route('feed.ajax.savesheetlink') }}',
                cache: false,
                dataType: "json",
                data: {
                    link : link,
                    id : id,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (res) {
                if(res.msg === 'failed') {
                    $('#modal_sheet').modal('hide');
                    $('#title').html('Ошибка!');
                    $('#msg').text(res.reason);
                    $('#modal_messages').modal('show');
                }
                if (res.msg === 'success') {
                    $('#modal_sheet').modal('hide');
                    $('#sheetlink').attr('href', res.link);
                }
            });
        });

        $(document).on('change', '#togglestatus', function(){
            let status = 0;
            if ($('#togglestatus').is(':checked')) {
                status = 1;
            }
            let id = '{{$parser->id}}';

            $.ajax({
                type: "POST",
                url: '{{ route('feed.ajax.togglestatus') }}',
                cache: false,
                dataType: "json",
                data: {
                    status : status,
                    id : id,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (res) {
                $('#status_updated').html(res.updated.date);
                console.log('Status toggle success');
            });
        });
        $(document).on('click', '.save', function(){
           let name = $('#name').val();
           let selector = $('#selector').val();
           let type = $('#type').val();
           let item = $('#item').val();
           let id = '{{$parser->id}}';
           $.ajax({
               type: "POST",
               url: '{{ route('feed.ajax.saveselector') }}',
               cache: false,
               dataType: "json",
               data: {
                   name : name,
                   selector: selector,
                   type: type,
                   item: item,
                   id : id,
                   _token:$('meta[name="csrf-token"]').attr('content')
               }
           }).done(function (res) {
               if (res.dublicate == '1') {
                   toastr.success('Селектор с таким названием уже существует!');
               } else {
                   toastr.success('Селектор сохранен!');
                   window.location.reload();
               }
           });
        });
        $(document).on('click', '.delete', function() {
            let selector_id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: '{{ route('feed.ajax.deleteselector') }}',
                cache: false,
                dataType: "json",
                data: {
                    selector_id : selector_id,
                    _token:$('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (res) {
                if (res.msg == 'success') {
                    toastr.success('Селектор удален!');
                    $('#row_'+selector_id).remove();
                }
            });
        });
        $(document).on('click', '.test', function () {
            let id = $(this).attr('data-id');
            let selector = $('#selector_'+id).html();
            let retest = 1;
            link = $('#testlink').val();
            $.ajax({
                type: "POST",
                url: '{{ route('feed.ajax.testselector') }}',
                cache: false,
                dataType: "json",
                data: {
                    id: id,
                    selector : selector,
                    link: link,
                    retest:retest,
                    _token:$('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (res) {
                if (res.status === 1) {
                    $('#result_'+id).html(res.data);
                    toastr.success('Данные получены!');
                }
                if (res.status === 0) {
                    $('#result_'+id).html('Данные не получены');
                    toastr.error('Данные не получены! Измените селектор!');
                }
            });
        });
        $(document).on('focusout', '#base_url', function () {
            let base_url = $('#base_url').val();
            $.ajax({
                type: "POST",
                url: '{{ route('feed.ajax.check') }}',
                cache: false,
                dataType: "json",
                data: {
                    base_url : base_url,
                    _token:$('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (res) {
                if (res.code === 200) {
                    $('#base_url_label').html('Базовый URL <span class="material-icons small text-success"></span>' + ' ' + '<span class="text-success">'+res.code+'</span>');
                } else {
                    $('#base_url_label').html('Базовый URL <span class="material-icons small text-danger">warning</span>' + ' ' + '<span class="text-danger">'+res.code+'</span>');
                }
            });
        });
        $(document).on('focusout', '#testlink', function () {
            let link = $('#testlink').val();
            $.ajax({
                type: "POST",
                url: '{{ route('feed.ajax.testlink') }}',
                cache: false,
                dataType: "json",
                data: {
                    link : link,
                    id: '{{ $parser->id }}' ,
                    _token:$('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (res) {
                if (res.status === 1) {
                    $('#testlink').addClass('text-white');
                }
            });
        });
        $(document).on('focusout', '#selector', function () {
            let selector = $('#selector').val();
            let link = $('#testlink').val();
            let type = $('#type').val();
            let item = $('#item').val();
            let retest = 0;
            $.ajax({
                type: "POST",
                url: '{{ route('feed.ajax.testselector') }}',
                cache: false,
                dataType: "json",
                data: {
                    selector : selector,
                    link: link,
                    type: type,
                    item: item,
                    retest:retest,
                    _token:$('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (res) {
                if (res.status === 1) {
                    $('#result').html(res.data);
                    toastr.success('Данные получены!');
                }
                if (res.status === 0) {
                    $('#result').html('Данные не получены');
                    toastr.error('Данные не получены! Измените селектор!');
                }
            });
        });
    </script>
@endpush
